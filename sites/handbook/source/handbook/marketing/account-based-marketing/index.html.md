---
layout: handbook-page-toc
title: Account Based Marketing
description: Account Based Marketing Handbook
twitter_image: /images/tweets/handbook-marketing.png
twitter_site: '@gitlab'
twitter_creator: '@gitlab'
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> Account Based Marketing Team

## Overview
{: #overview .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

To be added

### Goals
{: #goals}
<!-- DO NOT CHANGE THIS ANCHOR -->

The goals for new first-order MQLs and SAOs are derived from workbacks of sales pipeline projections. These differ by segment and geo, and can be discussed internally.

### Account Based Marketing Key Metrics
{: #key-metrics}
<!-- DO NOT CHANGE THIS ANCHOR -->

- **North Star Metric:** 
- **Efficiency Metric:** 
- **Business Impact Metric:** 
- **Supporting/Activity Metrics:** 

### OKRs
{: #okrs}
<!-- DO NOT CHANGE THIS ANCHOR -->

**OKRs = Objective + Key Result**

<details>
<summary markdown='span'>
FY22-Q3
</summary>

- **To Be Added**

</details>

## Team Structure
{: #team-structure .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

To be added

### Responsibilities
{: #responsibilities}
<!-- DO NOT CHANGE THIS ANCHOR -->

- Developing (in partnership with sales and marketing) and maintaining our [ideal customer profile (ICP)](/handbook/marketing/revenue-marketing/account-based-strategy/#ideal-customer-profile)
- Maintenance of GitLab's [focus account lists](/handbook/marketing/revenue-marketing/account-based-strategy/#focus-account-lists)
- [Account based marketing (ABM)](/handbook/marketing/revenue-marketing/account-based-strategy/#account-based-marketing-abm) campaigns and tactics
- [Demandbase](/handbook/marketing/revenue-marketing/account-based-strategy/#demandbase)
    - platform management
    - enablement
    - account scoring
    - account based engagement
    - campaign orchestration

### Meet the Team
{: #meet-the-team}
<!-- DO NOT CHANGE THIS ANCHOR -->

To be added

## Communication
{: #communication .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

In line with GitLab's overall [communication guidelines](https://about.gitlab.com/handbook/communication/), campaign managers work through MRs first and issues second (in order to preserve documentation), and our slack channel [#marketing_programs](https://gitlab.slack.com/archives/CCWUCP4MS) is used for team updated and quick questions.

The [#demand-gen](https://gitlab.slack.com/archives/CJFB4T7EX) channel is used for weekly check-ins and interaction with the larger demand generation team.

In between bi-weekly Campaigns Team Connect calls:
* **Updates** should be added to MRs when possible
   - Add to #marketing_programs Slack
   - Begin with `UPDATE: <brief description>` (i.e. `UPDATE: Marketo segmentation live for sales segment`)
   - Include link to MR (preferred) or issue
* **Topics** that need input should be added as an MR when possible, with issues as a secondary option, and pinged in Slack with `DISCUSS`
   - Add to #marketing_programs Slack
   - Begin with `DISCUSS: <brief description>` (i.e. `DISCUSS: Marketing tool champions`)
   - Include link to MR (preferred) or issue

### Meeting Cadence
{: #meeting-cadence}
<!-- DO NOT CHANGE THIS ANCHOR -->

Most of our team meetings are recorded and can be found [here](https://drive.google.com/drive/u/1/folders/1GDkvqVhimLDnX744eh9YS6_qHhKRafmJ).

- Bi-weekly Tuesdays - Campaign Team Connect Call (campaigns)
- Bi-weekly Wednesdays - Demand Generation Team Call (campaigns, digital, partner)
- Thursdays - Marketing strategy & tactics call (all marketing)

### The Handbook
{: #handbook}
<!-- DO NOT CHANGE THIS ANCHOR -->

Is our single source of truth (SSoT) for processes and relevant links

- Individual teams should link back to these SSoT sections to avoid confusion
- Collaborative tactics contain their own handbook pages
- The handbook will be iterated on as we establish and optimize processes for optimal efficiency

## Project Management
{: #project-management .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->


### Prioiritization
{: #prioritization-boards}
<!-- DO NOT CHANGE THIS ANCHOR -->

1. [ABM Team - Prioritization]()
   - At start of milestone, ensure that issues are in the proper category of prioritization. Align them to the [priority definitions](/handbook/marketing/account-based-marketing/#prioritization-labels).
   - Consider the time you have available in a 2 week period, recognizing meeting commitments and planned PTO.
   - Only add issues to account for 70% of your available work hours. Don't max out at 100% since there will likely be things that arise and need immediate attention throughout milestone.
   - *Ask yourself: considering realistic time this milestone, am I overcommitting? The first to drop from a milestone should be "Low" priority issues based on [definitions](/handbook/marketing/account-based-marketing/#prioritization-labels).*
   - Discuss milestone with Jackie and if need help determining what to drop and how to communicate (potentially) to issue requestors.
1. [ABM Team - Current Milestone](https://gitlab.com/groups/gitlab-com/marketing/demand-generation/-/boards/2574635?milestone_title=%23started&)
   - Minimize the "Low" and "Medium" columns. Your focus should first be on the "Top" and "High" priority issues.
   - Prioritize completion of "Top" priority issues, then "High" priority issues.

### Boards

* [Overall ABM Prioritization](https://gitlab.com/groups/gitlab-com/marketing/-/boards/3536730?label_name[]=ABM)
* [Current Milestone Prioritization](https://gitlab.com/groups/gitlab-com/marketing/-/boards/3536724?scope=all&label_name[]=ABM&assignee_username=megan_odowd)
   - [Megan's Current Milestone Priorities](https://gitlab.com/groups/gitlab-com/marketing/-/boards/3536724?scope=all&label_name[]=ABM&assignee_username=megan_odowd)
   - [Christina's Current Milestone Priorities](https://gitlab.com/groups/gitlab-com/marketing/-/boards/3536724?scope=all&label_name[]=ABM&assignee_username=cmcleod516) 

### Labels
{: #labels}
<!-- DO NOT CHANGE THIS ANCHOR -->

#### Prioritization labels
{: #prioritization-labels}
<!-- DO NOT CHANGE THIS ANCHOR -->

- `ABM-Priority::Top`: Immediate action needed, aligned to OKRs, ABM strategy, or top leadership asks. This category will be limited because not everything can be a priority. Timeframe: Immediate action needed.
- `ABM-Priority::High`: Not requiring immediate action, but aligned to OKRs, ABM strategy, or leadership asks. Timeframe: Within weeks.
- `ABM-Priority::Med`: Requests submitted that align to OKRs or ABM strategy. Process improvements to fix broken processes, or improve ABM team efficiency. Timeframe: Within months.
- `ABM-Priority::Low`: Requests submitted that would be helpful, but can be pushed for higher priority issues. Nice-to-have improvements for ABM team processes. Timeframe: No specific timeline.
- `ABM-Priority::TBD`: Requests that have not yet been prioritized by ABM.

#### Team labels
{: #team-labels}
<!-- DO NOT CHANGE THIS ANCHOR -->

* `mktg-intmktg`: DRI is in Integrated Marketing team
* `abm`: DRI is in ABM team
* `abm-request`: Issue requesting ABM team support (did not originate from DRI team)

#### Status labels
{: #intake-labels}
<!-- DO NOT CHANGE THIS ANCHOR -->

- `mktg-status::triage`: the issue will be evaluated to determine if full details are included, and ask questions as needed if the scope/details are not clear
- `mktg-status::blocked`: there were insufficient details in the issue for work to be triaged
    - The requester must provide the details and then move the status back to `mktg-status::triage` in order for the request to be reviewed again.
    - Please note: issue submission does not begin the SLA timeline; sufficient details begin the SLA timeline. [see note about turnaround time and SLAs](/handbook/marketing/account-based-marketing/#slas)
- `mktg-status::wip`: the issue is assigned and moved into appropriate time-based milestone
- `mktg-status::plan`: this is used for work that is in an earlier stage of planning and a specific request has not been made
    - This will likely be used mainly by ABM team members on issues that are not high priority but there is a desire to capture the idea/notes.

### Turnaround Time and SLAs
{: #slas}
<!-- DO NOT CHANGE THIS ANCHOR -->

⏱ **The SLA (Service Level Agreement) begins when all details (including 100% final copy) is provided in the request issue.**
- Tip: Submit issues with full "Submitter Details" complete
- Tip: Bookmark a view of your issues in Blocked status ([example](https://gitlab.com/groups/gitlab-com/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-status%3A%3Ablocked&assignee_username[]=jgragnola))

**Copy must be 100% final in order to be triaged.** This includes final edits for copy, grammar, sentence structure and readability, as well as review by relevant stakeholders (such as managers, product mktg, content mktg, sales, etc).

The issue requester is responsible for ensuring that appropriate `mktg-status` labels are applied to the issue (issue templates have the tags built in to assist).

**The due date applied when the issue is submitted is not final.** The [workback calculator](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029) is a guideline to assist your project planning, however if details are not provided in the issue 5 business days prior by requestor, the issue timeline will be at risk and likely to be pushed out.
  - If an issue due date is prior to the SLA (based on date "submitter details" were complete), the due date will be changed to meet the SLA timeline (5 BD from date of final details submitted)
  - For example, if the issue is submitted 0ct 1, with a due date of Oct 15, BUT details are not submitted until Oct 14, THEN the issue due date will be moved to Oct 21 by the triage manager.
  - The manager of ABM team and assignee are the ONLY individuals allowed to adjust due dates upon triage.
  - 🙏 Please **do not** ask ABM team members to complete work in a shorter timeline as this is disruptive to their milestone, and their priority is to plan, implement, and optimize ABM campaigns and tactics. They will manage their milestones appropriately and pull in work as bandwidth allows, and based on the SLA.

### Issue Templates
{: #issue-templates}
<!-- DO NOT CHANGE THIS ANCHOR -->

We ask that teams request work using the following issue templates in the ABM project. These templates more clearly indicate what information and details are required in order for the request to be triaged and fit into an existing milestone.

Note on timelines: Requests will be committed to as bandwidth permits. Please see section on [turnaround time & SLAs](/handbook/marketing/account-based-marketing/#slas).

#### Request Issue Templates
{: #request-issue-templates}
<!-- DO NOT CHANGE THIS ANCHOR -->

To be added

## Budgeting

Allocadia

## Reporting

### PMG Paid Digital Reporting

The ABM team uses the [PMG Reporting Dashboard](https://datastudio.google.com/reporting/19WMqzyDxrl1fK3puZ3kI7Pzig3Oex_BL/page/p_32tuunsvlc?s=l-p946nzu0I) to track performance and metrics for all paid media campaigns run through the PMG and GitLab Digital team. This dashboard covers many field breakdowns that PMG has mapped based on specific UTM values and naming conventions. The dashboard breaks down performance from top to bottom by Channel, Budget, Segment, Geo, Content, Language and weekly & monthly comparisons. The top of the dashboard features several filters to customize your view.

#### Viewing ABM Campaigns

The default filter on the dashboard shows digital campaigns only. To show all paid campaigns, including ABM, select ALL fields under the **`Budget`** filter. Once the **`Budget`** filter is showing all values go to the **`Team`** filter. This dropdown will show multiple marketing departments. Select **`abm`** to see a summary of the ABM campaigns running or were previously ran depending on the date range.

If you want to see the performance and metrics for a single campaign you can select a specific campaign code under the **`Campaign Code`** filter. **`Campaign Code`** is the utm_campaign value added to our landing page URLs. Be sure to check your date range when selecting specific campaigns.

Note that refreshing your screen will reset the filters in the dashboard. You can also click **`Reset`** at the top right of the dashboard. If the data has trouble loading or is loading with erros, click **`Refresh Data`** in the menu bar at the top left of the dashboard. For more details on how to navigate the PMG reporting dashboard you can refer to this [issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/811#note_714815706).
