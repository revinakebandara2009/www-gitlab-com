---
layout: job_family_page
title: "Sales Development Marketing Operations"
---

## Levels

### Associate Sales Development Operations Manager

The Associate Sales Development Operations Manager reports to the Director, Sales Development Operations.

#### Associate Sales Development Operations Manager Job Grade 

The Associate Sales Development Operations Manager is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Associate Sales Development Operations Manager Responsibilities

* Marketing technology
  * Learning tools that the SDR team has in place to act as backup on various systems as needed
  * Providing oversight on GitLab issues boards - building weekly standup issues, creating biweekly milestone issues to ensure handbook updates are completed, documenting minutes from meetings, and ensuring labels exist on all Marketing issues
* Marketing data stewardship
  * Assist with data cleanliness and mis-routing in SFDC 
  * Outreach sequence and snippet maintenance and refreshment
  * Uncovering gaps in our prospecting key persona data and pulling those lists into SFDC from ZoomInfo and LinkedIn
* Marketing analysis
  * Providing support to team with ad hoc analysis and/or the underlying data as needed

#### Associate Sales Development Operations Manager Requirements

* Exemplary spoken and written English.
* Experience in sales and/or marketing teams of B2B software, Open Source software, and the developer tools space is preferred.
* Experience with marketing automation software a plus
* Experience with Salesforce CRM software helpful
* Familiarity with Git and repositories useful
* Proficient in Outreach
* Proficiency in MS Excel/ Google Sheets
* Team-centric
* Self starter, willing to read and watch in order to learn. (Be ready to learn and how to use GitLab and Git)
* Ability to use GitLab

### Sales Development Operations Manager (Intermediate)

The Sales Development Operations Manager (Intermediate) reports to the Director, Sales Development Operations.

#### Sales Development Operations Manager (Intermediate) Job Grade 

The Sales Development Operations Manager (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Sales Development Operations Manager (Intermediate) Responsibilities

* Sales Development technology
   * Maintain Sales Development tools (SFDC, Outreach, ZoomInfo, Demandbase & LI)  performance and optimize processes.
   * Create training documentation that guides the sales development team.
   * Train the sales development team on Sales Development software tools.
   * Audit use of sales development  tools with an eye towards continually improving how they are configured and used.
* Data and messaging stewardship
   * Be proactive and review common data issues that impact speed to lead and spearhead change needed through SDR issue creation and progression
   * Investigate data quality and continuously improve on data quality.
   * Continually measure and revamp Outreach primary collections. 
* Marketing data examination
   * Assist in measuring and investigating the different tools being used by the SDR team to inform best practices
   * Assist key business partners in measuring the effectiveness of SDR inbound and outbound motions

#### Sales Development Operations Manager (Intermediate) Requirements

* Extends that of the Associate Sales Development Operations Manager requirements
* Experience with modern marketing and sales development solutions such as Salesforce, LinkedIn Sales Navigator, Demandbase, Outreach & ZoomInfo.
* Outreach experience is required.
* You are obsessed with problem solving, making processes work and have a measurable impact on the Sales Development organisation.
* Ability to learn how to use GitLab and Git.

### Senior Sales Development Operations Manager

The Senior Sales Development Operations Manager reports to the Director, Sales Development Operations.

#### Senior Sales Development Operations Manager Job Grade 

The Senior Marketing Operations Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Sales Development Operations Manager Responsibilities

* Extends that of the Sales Development Operations Manager (Intermediate) responsibilities
* Oversee strategic and operational initiatives related to leads and early stage opportunities. Work  with other functions to drive performance improvements, continuously enhance the impact of the sales development  org and help the company continue along its fast growth trajectory.
* Collaborate across functions to structure problems, conduct analysis, and drive to solutions through a rigorous, data-driven process.
* Create and manage project plans with clearly defined deliverables and resources, coordinate work streams and dependencies, track and communicate progress, and identify obstacles and ensure they are addressed.

#### Senior Sales Development Operations Manager Requirements

* Extends that of the Sales Development Operations Manager (Intermediate) requirements
* Excellent spoken and written English.
* Progressive and demonstrated relevant experience, including more technical expertise.
* Excellent analytical and problem-solving skills.
* Exemplary oral and written communication skills, including ability to concisely present project deliverables.
* Ability to work successfully with little guidance.

### Manager, Sales Development Operations

The Manager, Sales Development Operations reports to the Director, Sales Development Operations.

#### Manager, Sales Development Operations Job Grade 

The Manager, Sales Development Operations is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Sales Development Operations Responsibilities

* Effectively influence, align, and collaborate with key functions, particularly Sales and Marketing.
* Recruit, develop and guide a team to execute on key pipeline generation strategies.

#### Manager, Sales Development Operations Requirements

* Progresive and demonstrated relevant experience with go-to-market operations strategy and business analytics.
* Ability to work collaboratively, internally and externally.
* Ability to manage/prioritize multiple projects and adapt to a changing, fast-paced environment.
* Demonstrated management skill and ability to oversee a team and work in a group environment, including collaboration with Senior/Executive level leadership.
* Possess exemplary interpersonal skills including influencing, negotiations and teamwork skills.
* Ability to think strategically, develop frameworks and platforms that ensure optimal and unfettered access to business tools, assets and capabilities.
* Superb analytical, problem-solving capability and help the team make sound conclusions.
* You believe in teamwork and are not afraid of rolling up your sleeves.

### Director, Sales Development Operations

The Director, Sales Development Operations reports to the AVP of Sales Development.

#### Director, Sales Development Operations Job Grade 

The Director, Sales Development Operations is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director, Sales Development Operations Responsibilities

* Define the Sales Development Operations vision and strategy that enables intelligent decisions through precise analysis
* Own the Sales Development operations roadmap, ensuring marketing technology is evaluated, selected, implemented, customized and more importantly used to meet the needs of the SDR organization
* Collaborate with Marketing operations business partners on process improvements and efficiencies
* Regularly inform stakeholders across marketing, sales, and finance of insights on marketing performance to ensure cross functional alignment on the best possible data-driven marketing strategy
* Oversee the management  of the SDR handbook updates as processes and programs change
* Track and report on the entire SDR funnel including lead volume, lead quality, velocity, conversion stage-to-stage, and average value; make sure funnel measures can be analyzed by key dimensions such as industry, geo, and persona
* Define strategies to optimize SAO and ARR conversions for both First Order and expand teams
* Work and enable SDR team managers to identify needs in process improvement 
* Ensure processes are designed, documented, understood, and followed in a way that improves customer experience and pipeline generation 
* Continually work to simplify systems and processes, avoiding customization bloat and over-engineered processes
* Manage, build, and lead a strong team by coaching and developing existing members and closing talent gaps where needed through acquisition of new team members.
* SDR DRI on selection of new vendors
* Design process changes to improve SDR’s experience and enhance productivity

#### Director, Sales Development Operations Requirements

* Proven and relevant management experience in a fast paced and developing Sales Development Organization
* Direct experience in enterprise software industry and SaaS
* Strong leadership presence
* Balance of strategic vision and tactical execution
* Exemplary analytic and strategic thinking capability
* Adept at managing multiple projects and deadlines
* Excellent planning, organizational and project management skills
* Extends that of the Senior Manager, Sales Development requirements
* Track record of leading a team that consistently meets/exceeds goals
* Demonstrated experience designing, implementing and an innovative sales development technology stack.
* Experience in successfully hiring and onboarding SDRs and SDR managers
* Experience building out new SDR team segments, including processes and strategies
* Excellent understanding of the GitLab SDR tech stack is required
* Proven track record of implementing and executing efficiency measures in a high volume lead flow model.
* Ability to understand the nuances required to lead effective outbound campaigns
* Leadership at GitLab
* Ability to use GitLab

## Performance Indicators
 
* MQL to SAO conversion rate through process improvements
* SDR productivity (SAO number/SDR)
Marketing efficiency ratio (we might need to think of an SDR efficiency ratio)
* SDR Tech Stack usage
 
## Career Ladder
 
The next step in the Sales Development Marketing Operations job family is yet to be defined.
 
## Hiring Process
 
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).
 
* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with one of our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the hiring manager.
* Candidates will then be invited to schedule an interview with 2 - 4 team members.
 
Additional details about our process can be found on our [hiring page](/handbook/hiring/).
